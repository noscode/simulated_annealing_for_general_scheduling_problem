CC=g++
CFLAGS=-c -Wall -O3 -std=c++11
LDFLAGS=
SOURCES=main.cpp schedule.cpp greedy_algorithm.cpp simulated_annealing.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=schedule

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f *.o

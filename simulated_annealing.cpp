#include "simulated_annealing.h"

#include "tools.h"

#include <random>
#include <cmath>
#include <algorithm>
#include <cassert>
#include <chrono>
#include <ctime>
#include <cstdlib>
#include <vector>

/////////////////////////////////// Auxiliary stuff ///////////////////////////

int rnd(double d)
{
  return (int)(d + 0.5);
}

// Step for simulated annealing to decide to take (or not) new state that is worse than old state.
bool force_transition_time(int delta, double temperature) {
	double probability = exp (- delta / temperature);

	srand ( time(NULL) );

	double value = ((double) rand() / (RAND_MAX)) + 1;

	if (value <= probability)
		return true;

	return false;
}

// Function for simulated annealing to decrease temperature.
inline double decrease_temperature(double initial_temperature, int iteration) {
	return initial_temperature - (initial_temperature - 1)/(7*initial_temperature + 3000)*iteration;
	//return pow(0.999, iteration)*initial_temperature;
}

void move_task(Schedule& schedule, int current_task_priority,
		int new_task_number_of_processor, int new_task_priority) {
	int saved_value = schedule.order[current_task_priority];
	if (new_task_priority > current_task_priority) {
		for (int i = current_task_priority; i < new_task_priority; i++){
			schedule.order[i] = schedule.order[i+1];
		}
	} else {
		for (int i = current_task_priority; i > new_task_priority; i--){
			schedule.order[i] = schedule.order[i-1];
		}
	}
	schedule.order[new_task_priority] = saved_value;
	schedule.get_task(new_task_priority).processor = new_task_number_of_processor;
}

int generate_new_task_priority(const Schedule& schedule,
		int current_task_priority) {
	const Task& moving_task = schedule.get_task(current_task_priority);

	int lowest_priority = -1;
	int uppermost_priority = schedule.tasks.size();

	//for (int i = 0; i < schedule.tasks.size(); i++) {
	//	std::cout << schedule.order[i] << " ";
	//}
	//std::cout << std::endl;

	for (auto it = moving_task.predecessors.begin();
			it != moving_task.predecessors.end(); it++) {
		int index = find(schedule.order.begin(), schedule.order.end(), *it)
			- schedule.order.begin();
		//std::cout << *it << std::endl;
		//std::cout << index << std::endl;
		//std::cout << std::endl;

		if (index < schedule.tasks.size()) {
			lowest_priority = std::max(lowest_priority, index);
		}
	}
	
	for (int i = 0; i < schedule.tasks.size(); i++) {
		if (schedule.get_task(i).predecessors.count(moving_task.id) != 0) {
			uppermost_priority = std::min(uppermost_priority, i);
		}
	}

	assert(lowest_priority <= uppermost_priority);

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator(seed);
	
	std::uniform_int_distribution<int> distr(lowest_priority + 1, uppermost_priority - 1);
	int new_task_priority = distr(generator); //generate new task1's priority
	return new_task_priority;
}



void generate_new_schedule_simple(Schedule& new_schedule) {
	debug_stream << "\nGenerate new state: \n";

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator(seed);

	std::uniform_int_distribution<int> distr1(1, new_schedule.number_of_tasks - 1);
	std::uniform_int_distribution<int> distr2(0, new_schedule.number_of_processors - 1);

	int current_task_priority = distr1(generator); //generate random task's priority (index in vector)
	int new_task_number_of_processor = distr2(generator); //generate random number of processor to move task
	
	int new_task_priority = generate_new_task_priority(new_schedule, current_task_priority);

	move_task(new_schedule, current_task_priority, new_task_number_of_processor, new_task_priority);

//	Change Start Time of tasks with higher priority to right value!!!
	std::vector<int> processors_load(new_schedule.number_of_processors, 0);
	int min_priority = std::min(current_task_priority, new_task_priority);

	for (int i = 0; i < new_schedule.order.size(); i++) {
		Task& cur_task = new_schedule.get_task(i);
		if (i >= min_priority) {
			int new_start_time = std::max(processors_load[cur_task.processor],
					calc_min_start_time(cur_task, new_schedule));
			
			cur_task.start_time = new_start_time;
		}

		processors_load[cur_task.processor] = cur_task.start_time + cur_task.processing_time;
	}

//!	printSolution("Have got:", new_state, n, m);
}

void generate_new_schedule_distribution(Schedule& new_schedule) {
	debug_stream << "\nGenerate new state: \n";

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator(seed);

	//check_valid(schedule);

	std::vector<int> weights(new_schedule.tasks.size());
	std::vector<int> enum_weights(new_schedule.number_of_processors, -1);
	for (int i = 0; i < new_schedule.number_of_processors; i++) {
		for (int j = new_schedule.order.size() - 1; j >= 0; j--) {
			if (new_schedule.get_task(j).processor == i) {
				enum_weights[i] = new_schedule.get_task(j).get_finish_time();
				break;
			}
		}

		assert(enum_weights[i] != -1);
	}
	for (int i = 0; i < new_schedule.tasks.size(); i++) {
		weights[i] = enum_weights[new_schedule.get_task(i).processor];
	}

	//std::discrete_distribution<int> distr1(weights.begin(), weights.end());
	// VC++2012
	std::discrete_distribution<> distr1(weights.size(),
		0.0, (double) (weights.size() - 1), [weights] (double v) {int i = rnd(v); return weights[i];});
	
	std::uniform_int_distribution<int> distr2(0, new_schedule.number_of_processors - 1);

	int current_task_priority = distr1(generator); //generate random task's priority (index in vector)
	int new_task_number_of_processor = distr2(generator); //generate random number of processor to move task
	
	int new_task_priority = generate_new_task_priority(new_schedule, current_task_priority);

	move_task(new_schedule, current_task_priority, new_task_number_of_processor, new_task_priority);

//	Change Start Time of tasks with higher priority to right value!!!
	std::vector<int> processors_load(new_schedule.number_of_processors, 0);
	int min_priority = std::min(current_task_priority, new_task_priority);

	for (int i = 0; i < new_schedule.order.size(); i++) {
		Task& cur_task = new_schedule.get_task(i);
		if (i >= min_priority) {
			int new_start_time = std::max(processors_load[cur_task.processor],
					calc_min_start_time(cur_task, new_schedule));
			
			cur_task.start_time = new_start_time;
		}

		processors_load[cur_task.processor] = cur_task.start_time + cur_task.processing_time;
	}

//!	printSolution("Have got:", new_state, n, m);
}

void generate_new_schedule_swap(Schedule& new_schedule) {
	debug_stream << "\nGenerate new state: \n";
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine generator(seed);

	std::uniform_int_distribution<int> distr1(1, new_schedule.number_of_tasks - 1);

	int current_task1_priority = distr1(generator); //generate random tasks' priorities 
	int current_task2_priority = distr1(generator);

	debug_stream << "wgrw" << std::endl;
	//check_valid(new_schedule);
	debug_stream << "wgrw" << std::endl;

	int current_task1_number_of_processor = new_schedule.get_task(current_task1_priority).processor;
	int current_task2_number_of_processor = new_schedule.get_task(current_task2_priority).processor;
	
	// Now we should find priority for moving tasks
	int new_task1_priority = generate_new_task_priority(new_schedule, current_task1_priority);

	debug_stream << "new priority " << new_task1_priority << std::endl;

	move_task(new_schedule, current_task1_priority, current_task2_number_of_processor, new_task1_priority);
	debug_stream << "rdfrds" << std::endl;
	//check_valid(new_schedule);
	debug_stream << "rdfrds" << std::endl;

	int new_task2_priority = generate_new_task_priority(new_schedule, current_task2_priority);

	debug_stream << "dagga" << std::endl;
	move_task(new_schedule, current_task2_priority, current_task1_number_of_processor, new_task2_priority);
	debug_stream << "dagga" << std::endl;

	// Change Start Time of tasks with higher priority to right value!!!
	std::vector<int> processors_load(new_schedule.number_of_processors, 0);
	int min_priority = std::min(current_task1_priority, std::min(current_task2_priority, std::min(new_task1_priority, new_task2_priority)));
	
	for (int i = 0; i < new_schedule.tasks.size(); i++) {
		Task& cur_task = new_schedule.tasks[new_schedule.order[i]];
		if (i >= min_priority){
			int new_start_time = std::max(processors_load[cur_task.processor],
					calc_min_start_time(cur_task, new_schedule));
			cur_task.start_time = new_start_time;
		}
		processors_load[cur_task.processor] = cur_task.start_time + cur_task.processing_time;
	}

	//	printSolution("Have got:", new_state, n, m);
}

/////////////////////////////////// Main function /////////////////////////////

Schedule simulated_annealing(const Schedule& schedule,
		double initial_temperature, double final_temperature,
		GenerateNewScheduleOptions option, int MAX_ITERATIONS) {
	double current_temperature = initial_temperature;

	Schedule current_schedule(schedule);
	Schedule new_schedule(schedule);
	Schedule best_schedule(schedule);

	int iteration = 0;

	while (iteration < MAX_ITERATIONS && current_temperature > final_temperature) {
		//std::cout << "checking current_schedule... ";
		//check_valid(current_schedule);
		//std::cout << "OK." << std::endl;
		//std::cout << "checking new_schedule... ";
		//check_valid(new_schedule);
		//std::cout << "OK." << std::endl;
		//std::cout << "checking best_schedule... ";
		//check_valid(best_schedule);
		//std::cout << "OK." << std::endl;

		for (int i = 0; i < current_schedule.order.size(); i++){
			new_schedule.order[i] = current_schedule.order[i];
		}
		for (int i = 0; i < current_schedule.number_of_tasks; i++){
			new_schedule.tasks[i].start_time = current_schedule.tasks[i].start_time;
			new_schedule.tasks[i].processor = current_schedule.tasks[i].processor;
		}

		switch (option) {
			case GENERATE_SIMPLE:
				generate_new_schedule_simple(new_schedule);
				break;
			case GENERATE_DISTRIBUTION:
				generate_new_schedule_distribution(new_schedule);
				break;
			case GENERATE_SWAP:	
				generate_new_schedule_swap(new_schedule);
				break;
		}

		debug_stream << "Iteration #" << iteration << ". Old length: " << current_schedule.tasks.back().start_time << std::endl;

		//std::cout << "iteration " << iteration << std::endl;

		if(new_schedule.tasks.back().start_time <= current_schedule.tasks.back().start_time) {
			current_schedule = new_schedule;
			debug_stream << "WE'VE GOT THIS SOLUTION!" << std::endl;
			debug_stream << new_schedule.tasks.back().start_time << std::endl;

		} else {
			int delta = new_schedule.tasks.back().start_time - current_schedule.tasks.back().start_time;
			if (force_transition_time(delta, current_temperature)) {
				current_schedule = new_schedule;
				debug_stream << "WE'VE GOT THIS SOLUTION!" << std::endl;
				debug_stream << new_schedule.tasks.back().start_time << std::endl;
			} else {
				debug_stream << "We don't get this solution...";
			}
		}

		iteration++;

		current_temperature = decrease_temperature(initial_temperature, iteration);
		if (new_schedule.tasks.back().start_time <= best_schedule.tasks.back().start_time) {
			best_schedule = new_schedule;
		}

		//std::cout << "simulated_annealing ITERATION #" << iteration << std::endl;
		//std::cout << "current_schedule:" << std::endl;
		//print_schedule("", current_schedule);
		//std::cout << "new_schedule:" << std::endl;
		//print_schedule("", new_schedule);
		//std::cout << "best_schedule:" << std::endl;
		//print_schedule("", best_schedule);

	}

	return best_schedule;

}

#pragma once

#include "schedule.h"

enum GenerateNewScheduleOptions {GENERATE_SIMPLE,
	GENERATE_DISTRIBUTION, GENERATE_SWAP};

Schedule simulated_annealing(const Schedule& schedule,
		double initial_temperature, double final_temperature,
		GenerateNewScheduleOptions option, int MAX_ITERATIONS);

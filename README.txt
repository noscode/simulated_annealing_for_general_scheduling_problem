Programm builds schedule using simulated annealing for two types of initial data: 
1) proto???.stg: 	 contains number of tasks, number of processors and graph of predecessors. Also has one fictive (final) task.
2) rand????.stg: 	 contains number of tasks and graph of predecessors. Also has two fictive tasks (first and final ).

There are three modifications of simulated annealing implemented here. They are called (for whatever reason): "simple", "distribution" and "swap". The last is best for mean error, so is default.

So there are arguments:
--------------------------------------------------------------------------------
schedule.exe proto/rand number_of_processors path_to_file type_of_SA
--------------------------------------------------------------------------------
number_of_processors - ONLY for rand!!!
type_of_SA isn't nessesary

Some examples:
Windows:
schedule.exe proto initial_data\proto143.stg
schedule.exe proto initial_data\proto256.stg distribution
schedule.exe rand 2 initial_data\50\rand0111.stg
schedule.exe rand 8 initial_data\50\rand0111.stg
schedule.exe rand 8 initial_data\50\rand0111.stg simple

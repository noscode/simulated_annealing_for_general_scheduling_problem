#pragma once

#include <iostream>

#define DEBUG_DISABLED true

#define debug_stream \
if (DEBUG_DISABLED) {} \
else std::cout

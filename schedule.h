#pragma once

#include <set>
#include <vector>
#include <cassert>
#include <string>

class Task {
	public:
		const int id;

		int processing_time;
		int start_time;

		int processor;

		bool done;

		std::set<int> predecessors;

		Task(int _id);

		Task(const Task& other);

		Task& operator=(const Task& other);

		Task& operator=(Task&& other);

		int get_finish_time() const;

};

struct Schedule {
	int number_of_tasks;
	int number_of_processors;

	std::vector<Task> tasks;
	std::vector<int> order;

	Schedule();
	Schedule(const Schedule& other);

	Task& get_task(int i);
	const Task& get_task(int i) const;
};

void print_schedule(std::ostream& output, const std::string& message, const Schedule& schedule);

void check_valid(const Schedule& schedule);

int calc_min_start_time(const Task& task, const Schedule& schedule);

bool available(const Task& task, const Schedule& schedule);

bool gtr(const Task& task1, const Task& task2, const Schedule& schedule);

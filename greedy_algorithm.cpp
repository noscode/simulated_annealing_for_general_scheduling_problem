#include "greedy_algorithm.h"

#include <vector>
#include <cassert>
#include <algorithm>

#include "tools.h"

// Auxiliary function.
int max_avaliable_element_index(const Schedule& schedule){
	int index = 0;
	for (int i = 1; i < schedule.tasks.size(); i++){
		if (gtr(schedule.tasks[i], schedule.tasks[index], schedule)) {
			index = i;
		}
	}
	return index;
}

// Greedy algorithm to get initial solution. Tasks will be sorted in solution vector by order algorithm put them on some processor
// Commented lines with ! in the begining - to print steps.
void greedy_algorithm(Schedule& schedule) {

	// For each processor it will contain time when the processor
	// will be free to take another task.
	std::vector<int> processors_load(schedule.number_of_processors, 0);

	std::vector<int> /*WOLFENSTEIN THE*/new_order;

	debug_stream << "GREEDY ALGORITHM\n";

	while (!schedule.tasks.back().done) {
		int max_task_index = max_avaliable_element_index(schedule);
		Task& max_task = schedule.tasks[max_task_index];

		assert(available(max_task, schedule));

		int min_load_processor = min_element(processors_load.begin(), processors_load.end()) - processors_load.begin();
		
		int min_start_time = calc_min_start_time(max_task, schedule);
		int min_start_time_on_processor = processors_load[min_load_processor];
		int start_time = std::max(min_start_time, min_start_time_on_processor);
		
		max_task.start_time = start_time;
		max_task.processor = min_load_processor;
		max_task.done = true;

		processors_load[min_load_processor] = max_task.get_finish_time();
		new_order.push_back(max_task_index);

		debug_stream << "Task #" << max_task.id << "'s start time is " << start_time << "\n";

	}
	debug_stream << "TOTAL LENGTH = " << schedule.tasks.back().get_finish_time() << "\n\n";  

	schedule.order = new_order;
}

#include "schedule.h"

#include <iostream>
#include <cassert>
#include <algorithm>
#include <numeric>

////////////////////////////////// Task methods ////////////////////////////////

Task::Task(int _id) : id(_id) {
	done = false;
}

Task::Task(const Task& other) : id(other.id) {
	processing_time = other.processing_time;
	start_time = other.start_time;
	processor = other.processor;
	done = other.done;
	predecessors = other.predecessors;
}

Task& Task::operator=(const Task& other) {
	if(&other == this) return *this;
	processing_time = other.processing_time;
	start_time = other.start_time;
	processor = other.processor;
	done = other.done;
	predecessors = other.predecessors;
	return *this;
}

Task& Task::operator=(Task&& other) {
	processing_time = other.processing_time;
	start_time = other.start_time;
	processor = other.processor;
	done = other.done;
	predecessors = std::move(other.predecessors);
	return *this;
}

int Task::get_finish_time() const {
	return start_time + processing_time;
}

////////////////////////////// Schedule methods ///////////////////////////////

Schedule::Schedule() {
	number_of_tasks = 0;
	number_of_processors = 0;
}

Schedule::Schedule(const Schedule& other) {
	number_of_tasks = other.number_of_tasks;
	number_of_processors = other.number_of_processors;
	tasks = other.tasks;
	order = other.order;
}

Task& Schedule::get_task(int i) {
	return tasks[order[i]];
}

const Task& Schedule::get_task(int i) const {
	return tasks[order[i]];
}

///////////////////////////////// Tools ///////////////////////////////////////


void print_schedule(std::ostream& output, const std::string& message, const Schedule& schedule) {
	output << message << std::endl;

	for (int i = 0; i < schedule.number_of_processors; i++) {
		output << "Processor #"<< i << ": ";
		for (int j = 0; j < schedule.order.size(); j++) {
			const Task& cur_task = schedule.tasks[schedule.order[j]];
			if (cur_task.processor == i) {
				output << cur_task.id << "(" << cur_task.start_time << ") ";
			}
		}
		output << std::endl;
	}
	output << std::endl;

	output << "Total length = " << schedule.tasks.back().get_finish_time() << std::endl;
}

void check_valid(const Schedule& schedule) {
	assert((schedule.tasks.size() == (schedule.number_of_tasks + 1)));
	assert((schedule.order.size() == (schedule.number_of_tasks + 1)));

	std::set<int> s1;
	s1.insert(schedule.order.begin(), schedule.order.end());
	std::vector<int> v2(schedule.number_of_tasks + 1, 0);
	// std::iota(v2.begin(), v2.end(), 0);
	// VC++2012
	for (int i = 0; i < v2.size(); i++) {
		v2[i] = i;
	}
	std::set<int> s2;
	s2.insert(v2.begin(), v2.end());

	assert(s1 == s2);

	for (int i = 0; i < schedule.tasks.size(); i++) {
		assert(schedule.tasks[i].id == i);
		const Task& cur_task = schedule.get_task(i);
		for (auto it = cur_task.predecessors.begin(); it != cur_task.predecessors.end(); it++) {
			int cur_predecessor_index = find(schedule.order.begin(), schedule.order.end(), *it) - schedule.order.begin();
			assert(cur_predecessor_index < i);
		}

	}
}

int calc_min_start_time(const Task& task, const Schedule& schedule) {
	int time = 0;

	for (auto it = task.predecessors.begin(); it != task.predecessors.end(); it++) {
		time = std::max(time, schedule.tasks[*it].start_time + schedule.tasks[*it].processing_time);
	}

	return time;
}

bool available(const Task& task, const Schedule& schedule) {
	if (task.done) return false;

	for (auto it = task.predecessors.begin(); it != task.predecessors.end(); it++) {
		if (!schedule.tasks[*it].done) {
			return false;
		}
	}

	return true;
}

bool gtr(const Task& task1, const Task& task2, const Schedule& schedule) {
	if (available(task2, schedule)) {
		if (available(task1, schedule)) {
			return task1.processing_time > task2.processing_time;
		} else return false;
	} else {
		if (available(task1, schedule)) return true;
		else return task1.processing_time > task2.processing_time;
	}
	return false;
}

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <random>
#include <chrono>
#include <limits>
#include <cassert>
#include <set>
#include <cstring>

#include "tools.h"
#include "schedule.h"
#include "greedy_algorithm.h"
#include "simulated_annealing.h"


const int MAX_ITERATIONS = 10000;

void print_tasks(std::ostream& output, const Schedule& schedule) {
	for (int i = 0; i < schedule.order.size(); i++) {
		const Task& task = schedule.tasks[schedule.order[i]];
		const std::set<int>& predecessors = task.predecessors;

		output << task.id + 1 << " " << task.processing_time << " ";

		output << predecessors.size() << " ";
		for (auto it = predecessors.begin(); it != predecessors.end(); it++) {
			output << *it + 1 << " ";
		}
		output << std::endl;
	}
}

void load_rand_file(Schedule& schedule, std::istream& input, int num_of_processors) {
	input >> schedule.number_of_tasks;
	schedule.number_of_tasks++;
	schedule.number_of_processors = num_of_processors;

	std::cout << "We have " << schedule.number_of_tasks - 1
		<< " tasks and " << schedule.number_of_processors
		<< " processors" << std::endl;

	schedule.tasks.reserve(schedule.number_of_tasks + 1);
	schedule.order.resize(schedule.number_of_tasks + 1);

	for (int i = 0; i < schedule.number_of_tasks + 1; i++) {
		int id, number_of_predecessors;

		input >> id;

		Task task(id);

		input >> task.processing_time;

		input >> number_of_predecessors;
		for (int j = 0; j < number_of_predecessors; j++) {
			int predecessor;
			input >> predecessor;
			task.predecessors.insert(predecessor);
		}

		task.start_time = -1;
		task.processor = -1;

		schedule.tasks.push_back(task);
	}

	//std::iota(schedule.order.begin(), schedule.order.end(), 0);
	// VC++2012
	for (int i = 0; i < schedule.number_of_tasks + 1; i++) {
		schedule.order[i] = i;
	}
}

void load_proto_file(Schedule& schedule, std::istream& input) {
	input >> schedule.number_of_tasks >> schedule.number_of_processors;

	std::cout << "We have " << schedule.number_of_tasks
		<< " tasks and " << schedule.number_of_processors
		<< " processors" << std::endl;

	schedule.tasks.reserve(schedule.number_of_tasks + 1);
	schedule.order.resize(schedule.number_of_tasks + 1);

	for (int i = 0; i < schedule.number_of_tasks + 1; i++) {
		int id, number_of_predecessors;

		input >> id;

		Task task(id-1);

		input >> task.processing_time;

		input >> number_of_predecessors;
		for (int j = 0; j < number_of_predecessors; j++) {
			int predecessor;
			input >> predecessor;
			task.predecessors.insert(predecessor-1);
		}

		task.start_time = -1;
		task.processor = -1;

		schedule.tasks.push_back(task);
	}

	//std::iota(schedule.order.begin(), schedule.order.end(), 0);
	// VC++2012
	for (int i = 0; i < schedule.number_of_tasks + 1; i++) {
		schedule.order[i] = i;
	}
}

//Generate PostScript file with shedule
void draw_solution(std::ofstream& output, const Schedule& schedule) {
	int x_indent = 100;
	int y_indent = 100;
	int x = 0;
	int y = y_indent;
	int rec_scale = 20;
	int rec_height = rec_scale;
	int max_x = rec_scale * schedule.get_task(schedule.order.size() - 1).get_finish_time();

	int m = schedule.number_of_processors;

	output << "%!PS-Adobe-2.0" << std::endl;
	output << "570 " << max_x << " div\ndup\nscale\n\n";

	output << "/Times-Roman findfont\n10 scalefont\nsetfont\n\n";
		
	output <<"0.8 setgray\n" << x_indent << " "<< y_indent << " "
		<< max_x << " " << rec_height * m << " " << " rectfill\n0 setgray\n"; 
	output <<"4 setlinewidth\n"  << x_indent / 2 << " "<< y_indent/2 << " "
		<< max_x + x_indent  << " " << rec_height * m  + y_indent << " "
		<< " rectstroke\n"; 
	output <<"1 setlinewidth\n";

	for (int cur_proc = 0; cur_proc < schedule.number_of_processors; cur_proc++) {
		for (int i = 0; i < schedule.order.size(); i++) {
			const Task& cur_task = schedule.get_task(i);
			if (cur_task.processor == cur_proc) {
				x = rec_scale * cur_task.start_time + x_indent;
				int rec_length = rec_scale * cur_task.processing_time;
				output << "1 setgray " << x << " " << y << " " <<
					rec_length <<" " << rec_height << " rectfill 0 setgray\n";
				output << x << " " << y << " " << rec_length <<
					" " << rec_height << " rectstroke\n"; 
				output << x - 7 + rec_length / 2  << " " << y + 7 <<
					" moveto\n" << " (" <<
					cur_task.id + 1 << ") show\n";
			}
		}
		y = y + rec_height;
	}

	y = rec_height * m + y_indent * 5 / 4;

	output << "newpath " << x_indent << " " << y << " moveto "
		<< max_x + x_indent << " " << y << " lineto stroke\n";
	output << "/Times-Roman findfont\n" << y_indent/5
		<< " scalefont\nsetfont\n\n";
	output << "newpath " << x_indent << " " << y - 4
		<< " moveto 0 8 rlineto stroke\n";
	output << x_indent - 4 << " " << y + 4 << " moveto (0) show\n";

	for (int i = 1; i <= schedule.tasks.back().get_finish_time()/100; i++) {
		output << "newpath " << i * 100 * rec_scale + x_indent
			<< " " << y - 4 << " moveto 0 8 rlineto stroke\n";
		output << i * 100 * rec_scale + x_indent - 15 << " "
			<< y + 6 << " moveto (" << i*100 << ") show\n";
	}

	output << "newpath " << max_x + x_indent << " " << y - 4
		<< " moveto 0 8 rlineto stroke\n";
	output << max_x + x_indent - 10 << " " << y + 4 << " moveto ("
		<< schedule.tasks.back().get_finish_time() << ") show\n";

}

int main(int argc, char* argv[]) {
	if (argc > 1){
		if ((argc > 2) && ((strcmp(argv[1], "rand")== 0) || strcmp(argv[1], "proto") == 0)){

			std::cout << "As Windows CMD shell with default configuration remembers only ~80 lines of output, it will be duplicated in \"output_log.txt\" file." << std::endl;
			std::ofstream output("output_log.txt");
			output << "As Windows CMD shell with default configuration remembers only ~80 lines of output, it will be duplicated in \"output_log.txt\" file." << std::endl;

			int argv_shift = 0;
			if (strcmp(argv[1], "rand")== 0){
				argv_shift++;
			}

			std::cout << "Loading file \"" << argv[2 + argv_shift] << "\"..." << std::endl;
			output << "Loading file \"" << argv[2 + argv_shift] << "\"..." << std::endl;
			std::ifstream input(argv[2 + argv_shift]);
			Schedule schedule;
			
			if (strcmp(argv[1], "rand")== 0){
				int num_of_processors = atoi(argv[2]);
				load_rand_file(schedule, input, num_of_processors);
			}
			else if (strcmp(argv[1], "proto")== 0){
				load_proto_file(schedule, input);
			}

//			output << "Got:" << std::endl;
//			std::cout << "Got:" << std::endl;
//			print_tasks(std::cout, schedule);
//			print_tasks(output, schedule);			

			output << std::endl;
			std::cout << std::endl;

			output << "Building initial schedule using Greedy Algorithm..." << std::endl;
			std::cout <<  "Building initial schedule using Greedy Algorithm..." << std::endl;
			greedy_algorithm(schedule);
			print_schedule(output, "Initial schedule:", schedule);
			print_schedule(std::cout, "Initial schedule:", schedule);

			output << std::endl;
			std::cout << std::endl;

			output << "Now trying to improve initial solution with simulated annealing..." << std::endl;
			std::cout << "Now trying to improve initial solution with simulated annealing..." << std::endl;
			//double initial_temperature = schedule.number_of_tasks*schedule.number_of_tasks/1000.0;
			double initial_temperature = schedule.number_of_tasks;
			double final_temperature = 1;
			std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
			if ((argc == (3 + argv_shift)) || (((argc == (4 + argv_shift))) && (strcmp(argv[3 + argv_shift], "swap")== 0))){
				schedule = simulated_annealing(schedule, initial_temperature, final_temperature, GENERATE_SWAP, MAX_ITERATIONS);
			}
			else if ((argc == (4 + argv_shift)) && (strcmp(argv[3 + argv_shift], "simple")== 0)){
				schedule = simulated_annealing(schedule, initial_temperature, final_temperature, GENERATE_SIMPLE, MAX_ITERATIONS);
			}
			else if ((argc == (4 + argv_shift)) && (strcmp(argv[3 + argv_shift], "distribution")== 0)){
				schedule = simulated_annealing(schedule, initial_temperature, final_temperature, GENERATE_DISTRIBUTION, MAX_ITERATIONS);
			}
			else{
				std::cout << "print -h for help" << std::endl;
				return 0;
			}


			std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
			auto lasted = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
			output << "Simulated annealing lasted " << lasted << " milliseconds (" << ((int) ceil(initial_temperature*7 + 3000)) << " iterations)." << std::endl;
			std::cout << "Simulated annealing lasted " << lasted << " milliseconds (" << ((int) ceil(initial_temperature*7 + 3000)) << " iterations)." << std::endl;
			print_schedule(output, "Got schedule:", schedule);
			print_schedule(std::cout, "Got schedule:", schedule);
			
			std::cout << "Now drawing a picture of the final schedule..." << std::endl;
			output << "Now drawing a picture of the final schedule..." << std::endl;
			std::string picture_name = "schedule.ps";
			std::ofstream ps_output("schedule.ps");
			draw_solution(ps_output, schedule);
			std::cout << "Check out a picture of the final schedule in \"" << picture_name << "\"." << std::endl;
			output << "Check out a picture of the final schedule in \"" << picture_name << "\"." << std::endl;
			return 0;
		} else if ((strcmp(argv[1], "-h")== 0) || (strcmp(argv[1], "-help")== 0) || (strcmp(argv[1], "help")== 0)){
			std::cout << "Programm builds schedule using simulated annealing for two types of initial data: " << std::endl <<
				"1) proto???.stg: \t contains number of tasks, number of processors and graph of predecessors. Also has one fictive (final) task." << std::endl <<
				"2) rand????.stg: \t contains number of tasks and graph of predecessors. Also has two fictive tasks (first and final )." << std::endl <<
				std::endl <<
				"There are three modifications of simulated annealing implemented here. They are called (for whatever reason): \"simple\", \"distribution\" and \"swap\". The last is best for mean error, so is default." << std::endl <<
				std::endl <<
				"So there are arguments:" << std::endl <<
				"--------------------------------------------------------------------------------" << std::endl <<
				"schedule.exe proto/rand number_of_processors path_to_file type_of_SA" << std::endl <<
				"--------------------------------------------------------------------------------" << std::endl <<
				"number_of_processors - ONLY for rand!!!" << std::endl <<
				"type_of_SA isn't nessesary" << std::endl <<
				std::endl <<
				"Some examples:" << std::endl<<
				"Windows:" << std::endl <<
				"schedule.exe proto initial_data\\proto143.stg"<< std::endl <<
				"schedule.exe proto initial_data\\proto256.stg distribution"<< std::endl <<
				"schedule.exe rand 2 initial_data\\50\\rand0111.stg"<< std::endl <<
				"schedule.exe rand 8 initial_data\\50\\rand0111.stg"<< std::endl <<
				"schedule.exe rand 8 initial_data\\50\\rand0111.stg simple"<< std::endl;

		}  else {
			std::cout << "print -h for help" << std::endl;
		}
	} else {
		std::cout << "print -h for help" << std::endl;
	}

	return 0;
}
